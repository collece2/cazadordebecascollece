import java.util.*;
import java.io.*;

public class CazadorDeBecasCollece {
	static class Beca {
		private int id;
		private int mesi;
		private int mesf;
		private int sueldo;

		private double ganancia;// Para calcular el beneficio con distintas heurísticas con voraces.
								// Para backtracking no se usa.

		Beca(int i, int mi, int mf, int s) {
			id = i;
			mesi = mi;
			mesf = mf;
			sueldo = s;
			ganancia = (mf - mi + 1) * s;
		}

		int getId() {
			return id;
		}

		int getMesI() {
			return mesi;
		}

		int getMesF() {
			return mesf;
		}

		int getSueldo() {
			return sueldo;
		}

		double getGanancia() {
			return ganancia;
		}

		@Override
		public String toString() {
			return id + " - " + mesi + " - " + mesf + " - " + sueldo + " - " + ganancia;
		}
	}

	static class Becario {
		private ArrayList<Beca> listaBecas;

		public Becario() {
			setListaBecas();
		}

		public ArrayList<Beca> getListaBecas() {
			return listaBecas;
		}

		private ArrayList<Beca> leerfichero(String nombreFic) {

			ArrayList<Beca> ListaBecas = new ArrayList<Beca>();

			try {
				Scanner sc = new Scanner(new FileReader(nombreFic));
				while (sc.hasNext()) {
					int id = sc.nextInt();
					int mesi = sc.nextInt();
					int mesf = sc.nextInt();
					int sueldo = sc.nextInt();
					Beca aux = new Beca(id, mesi, mesf, sueldo);
					ListaBecas.add(aux);
				}
				sc.close();
			} catch (IOException ioe) {
				System.out.println("Error con el fichero de datos");
			}

			return ListaBecas;
		}

		private int leerentero(int min, int max) {
			Scanner TECLADO = new Scanner(System.in);
			System.out.println("Introduce un valor entre " + min + " y " + max);
			int res = TECLADO.nextInt();
			while (res < min || res > max) {
				System.out.println("Debe estar comprendido entre " + min + " y " + max);
				res = TECLADO.nextInt();
			}
			TECLADO.close();
			return res;
		}

		public void setListaBecas() {
			String nombreFic = "src//";
			System.out.println(
					"¿De qué fichero quieres cargar los datos?\n1.DatosBecas8_1 (8 becas) \n2.DatosBecas8_2 (8 becas) \n3.DatosBecas100 (100 becas)");
			int opcion = leerentero(1, 3);
			switch (opcion) {
			case 1:
				nombreFic = nombreFic + "DatosBecas4.txt";
				break;
			case 2:
				nombreFic = nombreFic + "DatosBecas8.txt";
				break;
			case 3:
				nombreFic = nombreFic + "DatosBecas100.txt";
				break;
			}
			this.listaBecas = leerfichero(nombreFic);
		}

		public void mostrarPosibilidades() {
			// GENERA TODAS LAS COMBINACIONES POSIBLES DE BECAS SIN QUE SE SOLAPEN. PARA
			// ELLO, HABRÁ QUE DEFINIR OTRO MÉTODO QUE RESUELVA EL PROBLEMA HACIENDO USO DE
			// BACKTRACKING.
			//
			// HAY QUE IMPLEMENTAR ESTE MÉTODO.
		}

		public void mostrarBecasUniversidad() {
			System.out.println("Becas que tiene para elegir");
			Iterator<Beca> itrArrayList = listaBecas.iterator();
			int posicion = 0;
			while (itrArrayList.hasNext()) {
				System.out.println("Beca(" + posicion + ") = " + itrArrayList.next());
				posicion++;
			}
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		Becario pepe = new Becario();
		pepe.mostrarBecasUniversidad();
		pepe.mostrarPosibilidades();
	}
}
